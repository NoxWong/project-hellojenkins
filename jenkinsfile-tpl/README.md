# jenkinsfile-tpl

CI/CD:
- **LAB 00:** [Init](./lab00_Jenkinsfile)
- **LAB 01:** [Build](./lab01_Jenkinsfile?plain=1#L12)
- **LAB 02:** [Test: UT & API Test](./lab02_Jenkinsfile?plain=1#L19)
- **LAB 03:** [Test: E2E Test (Selenium)](./lab03_Jenkinsfile?plain=1#L35)
- **LAB 04:** [Publish to container registry](./lab04_Jenkinsfile?plain=1#L50)
- **LAB 05:** [Deploy (Ansible)](./lab05_Jenkinsfile?plain=1#L64)

Secure CI/CD:
- **LAB 06:** [Init](./lab06_Jenkinsfile?plain=1#L19)
- **LAB 07:** [Secret Detection (gitleaks) ](./lab07_Jenkinsfile?plain=1#L21)
- **LAB 08:** [Container Scanning (Trivy)](./lab08_Jenkinsfile?plain=1#L29)
- **LAB 09:** [SAST (SonarQube)](./lab09_Jenkinsfile?plain=1#L41)
- **LAB 10:** [Final](./lab10_Jenkinsfile.final)