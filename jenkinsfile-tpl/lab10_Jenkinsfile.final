pipeline {
    agent any

    environment {
        VERSION = sh(
                returnStdout: true,
                script: "grep version package.json | awk '{print \$2}' | sed  's/[\"|,]//g'"
            )
        REGISTRY_URL = 'registry.gitlab.com'
        REGISTRY_PROJ = '<change-to-your-project!>'
    }

    stages {
        stage('build') {
            steps {
                sh 'echo "Building.., app version: $VERSION"'
                sh 'docker build --no-cache -t $JOB_NAME:$BUILD_NUMBER .'
            }
        }

        stage('test') {
            parallel {
                stage('unit_test') {
                    steps {
                        echo 'Running unit tests..'
                        sh 'docker run --rm $JOB_NAME:$BUILD_NUMBER /bin/sh -c "npm install --include=dev && npm test"'
                    }
                }

                stage('api_test') {
                    steps {
                        echo 'Running api tests..'
                        sh 'docker run --rm $JOB_NAME:$BUILD_NUMBER /bin/sh -c "npm install --include=dev && npm run testapi"'
                    }
                }

                stage('e2e_test') {
                    steps {
                        echo 'Running E2E tests..'
                        sh '''
                            docker run --rm -e NODE_ENV=production -e SELENIUM_SERVER=host -e PORT=3000 \
                                -v ${PWD}:/app -w /app registry.gitlab.com/cphtofficial/image-nodejs-chrome:1.0.0 \
                                /bin/bash -c "npm install --include=dev && npm run teste2e"
                        '''
                    }
                }
            }
        }

        stage('secure_test') {
            parallel {
                stage('secret_detection') {
                    steps {
                        echo 'Running secret detection..'
                        // Failed if files contain sensitive data
                        sh 'docker run --rm -v ${PWD}:/code ghcr.io/gitleaks/gitleaks:latest detect --no-git --source="/code" -v'
                    }
                }

                stage('container_scanning') {
                    steps {
                        echo 'Running container scanning..'
                        // Failed if container (scope: OS and application packages) has HIGH or CRITICAL vulnerabilities
                        sh '''
                            docker run --rm -v /var/run/docker.sock:/var/run/docker.sock \
                                aquasec/trivy --exit-code 1 --severity HIGH,CRITICAL --scanners vuln \
                                image $JOB_NAME:$BUILD_NUMBER
                        '''
                    }
                }

                stage('sast') {
                    steps {
                        echo 'Running SAST w/ SonarQube..'
                        // Failed if violating Quality Gates
                        withCredentials([usernamePassword(credentialsId: 'sq-demo-project', usernameVariable: 'SONAR_KEY', passwordVariable: 'SONAR_TOKEN')]) {
                            sh '''
                                docker run \
                                    --rm \
                                    --network=host \
                                    -e SONAR_HOST_URL="http://sonarqube:9000"  \
                                    -e SONAR_SCANNER_OPTS="-Dsonar.projectKey=$SONAR_KEY -Dsonar.exclusions=tests/** -Dsonar.qualitygate.wait=true" \
                                    -e SONAR_TOKEN=$SONAR_TOKEN \
                                    -v "${PWD}:/usr/src" \
                                    sonarsource/sonar-scanner-cli
                            '''
                        }
                    }
                }
            }
        }

        stage('publish') {
            when {
                expression { env.GIT_BRANCH == 'origin/main' }
            }
            steps {
                echo 'Publishing image..'
                withCredentials([usernamePassword(credentialsId: 'gitlab-project-hellojenkins-token', usernameVariable: 'REGISTRY_USER', passwordVariable: 'REGISTRY_TOKEN')]) {
                    sh 'echo $REGISTRY_TOKEN | docker login -u $REGISTRY_USER --password-stdin $REGISTRY_URL'
                    sh 'docker tag $JOB_NAME:$BUILD_NUMBER $REGISTRY_URL/$REGISTRY_PROJ:$VERSION'
                    sh 'docker push $REGISTRY_URL/$REGISTRY_PROJ:$VERSION'
                }
            }
        }

        stage('deploy') {
            when {
                expression { env.GIT_BRANCH == 'origin/main' }
            }
            agent {
                docker {
                    image 'cytopia/ansible:2.10'
                    args '-u root:root'
                }
            }
            steps {
                echo 'Deploying app..'
                withCredentials([
                    sshUserPrivateKey(credentialsId: 'production-auth', keyFileVariable: 'AUTH_KEY', usernameVariable: 'AUTH_USER'),
                    usernamePassword(credentialsId: 'gitlab-project-hellojenkins-token', usernameVariable: 'REGISTRY_USER', passwordVariable: 'REGISTRY_TOKEN')
                ]) {
                    sh '''
                        export ANSIBLE_HOST_KEY_CHECKING=False
                        ansible-playbook -i deployments/ansible/inventory -u $AUTH_USER --private-key $AUTH_KEY deployments/ansible/deploy.yml
                    '''
                }
            }
        }
    }

    post {
        cleanup {
            echo 'Cleanup..'
            script {
                if (env.GIT_BRANCH == 'origin/main') {
                    sh 'docker rmi $REGISTRY_URL/$REGISTRY_PROJ:$VERSION'
                }
                sh 'docker rmi $JOB_NAME:$BUILD_NUMBER'
            }
        }
    }
}