<h1>部署容器（Ansible）</h1>
透過 Ansible 將應用程式部署至正式環境。

<h2>📝 目錄</h2>

- [1. 前置作業](#1-前置作業)
  - [定義主機資訊](#定義主機資訊)
  - [配置環境變數](#配置環境變數)
- [2. 執行 Ansible 腳本](#2-執行-ansible-腳本)

---


# 1. 前置作業

## 定義主機資訊
1. 將欲部署的主機資訊列於 `inventory`。e.g.,
    ```
    [production]
    # Listing your production serverIP
    8.8.8.8 ansible_port=22
    ```
2. 準備 SSH 的登入帳號與 SSH Key

## 配置環境變數
腳本（`deploy.yml`）將會讀取以下的環境變數，因此需進行以下的配置：
```sh
# 映像檔儲存庫的 URL
$ export REGISTRY_URL=...

# 私人映像檔儲存庫的登入帳號與密碼
$ export REGISTRY_USER=...
$ export REGISTRY_TOKEN=...

# 映像檔名稱
$ export REGISTRY_PROJ=...
# 應用程式版本
$ export VERSION=...
```
e.g.,
```sh
$ export REGISTRY_URL=registry.gitlab.com

$ export REGISTRY_USER=gitlab+deploy-token-23
$ export REGISTRY_TOKEN=hithisistoken

$ export REGISTRY_PROJ=cphttraining/playground/app-nodejs
$ export VERSION=1.0.0
```

# 2. 執行 Ansible 腳本
 ```sh
 $ cd deployments/ansible

 $ ansible-playbook -i inventory -u <ssh-username> --private-key <path-of-ssh-key> deploy.yml
 ```
