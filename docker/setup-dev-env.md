<h1>建立 Docker 的開發環境</h1>

<h2>📝 目錄</h2>

- [建立與啟動容器](#建立與啟動容器)
  - [1. 建立開發環境](#1-建立開發環境)
    - [建立 NodeJS 環境](#建立-nodejs-環境)
    - [建立 NodeJS + Selenium 環境](#建立-nodejs--selenium-環境)
  - [2. 安裝套件](#2-安裝套件)
  - [3. 啟動服務](#3-啟動服務)
- [執行測試](#執行測試)
  - [單元測試](#單元測試)
  - [API 測試](#api-測試)
  - [E2E 測試（Selenium）](#e2e-測試selenium)

---
❗請確認您在 `app-nodejs` 的根目錄進行操作。
```sh
$ cd <the-path-to-app-nodejs>
```

# 建立與啟動容器
## 1. 建立開發環境
請<u>**擇一**</u>方式進行環境建置：

✴️ 不包含 E2E 測試 👉 [建立 NodeJS 環境](#建立-nodejs-環境)

✴️ 可基於 Selenium 進行 E2E 測試 👉 [建立 NodeJS + Selenium 環境](#建立-nodejs--selenium-環境)

### 建立 NodeJS 環境
以 docker compose 建立：
```sh
$ docker compose -f docker/base/docker-compose.dev.yml up -d

[+] Running 1/1
 ⠿ Container dev-app-nodejs  Started
```

### 建立 NodeJS + Selenium 環境
**方式一：使用 Selenium Grid Standalone with Chrome（有介面，但需維護 Selenium Grid）**

在遠端建立一個含有 Chrome 的環境，使測試可以在該遠端的環境中內進行：
```sh
$ docker compose -f docker/selenium/remote/docker-compose.se-remote.yml up -d

[+] Running 2/2
 ⠿ Container selenium-chrome  Started
 ⠿ Container dev-app-nodejs   Started
```

> 稍後執行 E2E 測試時，可透過 Selenium Grid 介面 (http://localhost:7900) 執行過程。


**方式二：直接在應用服務的容器內安裝瀏覽器所需的驅動程式與瀏覽器（無介面，但不需要再多維護 Selenium Grid）**
```sh
$ docker compose -f docker/selenium/host/docker-compose.se-host.yml up -d

[+] Running 1/1
 ⠿ Container dev-app-nodejs  Started
```

## 2. 安裝套件
1. 進入容器
    ```sh
    ## alpine
    $ docker exec -it dev-app-nodejs sh

    ## debian
    $ docker exec -it dev-app-nodejs bash
    ```
2. 安裝 npm 套件
   ```sh
   $ npm install
   ```

## 3. 啟動服務
1. 啟動服務
    ```sh
    $ npm start
    ```
2. 打開瀏覽器並輸入網址 (http://localhost:3000)

# 執行測試
## 單元測試
```sh
$ npm test
```
> 輸出：e.g.,
```sh
> app-nodejs@1.0.0 test
> mocha ./tests/ut --exit


  Test Login
    ✔ [驗證密碼] 合法使用者，須回傳驗證成功
    ✔ [驗證密碼] 非法使用者，須回傳驗證失敗
    ✔ [驗證歡迎訊息] 須回傳正確訊息


  3 passing (23ms)
```

## API 測試
```sh
$ npm run testapi
```
> 輸出：e.g.,
```sh
> app-nodejs@1.0.0 testapi
> mocha ./tests/api --exit

Node app is running at localhost:3000


  Test Login API
    ✔ [驗證身份] 合法使用者，須回傳 200 (106ms)
    ✔ [驗證身份] 非法使用者，須回傳 401


  2 passing (126ms)
```

## E2E 測試（Selenium）
```sh
$ npm run teste2e
```
> 輸出：e.g.,
```
> app-nodejs@1.0.0 teste2e
> mocha ./tests/E2E --exit

Node app is running at localhost:3000


  E2E Test: Login
[Selenium Webdriver] Preparing...
[Selenium Webdriver] Using remote Selenium Hub
[Selenium Webdriver] Get ready!
helloMsg: Hello, validuser! Welcome to our website.
    ✔ [驗證身份] 合法使用者，須看到歡迎訊息 (1122ms)
errorMsg: Wrong username or password. Try again!
    ✔ [驗證身份] 非法使用者，須看到資訊錯誤提醒 (656ms)
[Selenium Webdriver] Closing ...
[Selenium Webdriver] Closed!


  2 passing (3s)
```
> 採用方式一建立 Selenium Grid 者，可透過 Selenium Grid 介面 (http://localhost:7900) 查看執行過程。